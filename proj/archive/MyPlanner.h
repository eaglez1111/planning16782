#ifndef _MYPLANNER_H
#define _MYPLANNER_H

#include "utils.h"




/* Hyper Parameters */

#define step_size (PI/40)
#define K 30000
#define MAX_N_STEPS 4 //epsilon
#define INF_STEPS 9999 //practically infinity number of steps
#define GoalAttemptCnt 50
#define MAX_RADIUS PI/10
#define GAMMA 1.0
#define INF_RADIUS 9999*PI //practically infinitily large radius
#define ASTAR_W 0.0





/* For easier coding */

#include <chrono>
typedef std::chrono::high_resolution_clock Clock;
typedef pair<double, int> F_Id;
#define ANG_DIFF(A, B) ( fmod(A-B+PI+8*PI,2*PI)-PI )
#define ADVANCED 2
#define REACHED 1
#define TRAPPED 0
#define RES_MAX 16





/* Node and Planner Definition */

typedef struct{
  double* conf;
  int p_id;
  int* coord;
  double d;
  vector<int> n_id;
} node;


class MyPlanner {
  public:
    double*	map;
    int x_size;
    int y_size;
    double* armstart_anglesV_rad;
    double* armgoal_anglesV_rad;
    int numofDOFs;
    int planner_id;

    double* q_rand;
    int q_near_id;
    double* q_step;
    vector<node> Tree, Tree_r;

    double radius;
    double q_new_d, q_new_c;
    vector<int> X_near_id;
    int q_new_id;

    int n_node;
    vector<node> roadmap;
    int q_goal_id, q_start_id;
    int path_len;
    stack<int> path;

    // drrt parameters
    int n_link;
    int cnt = -1;
    int res = 1;
    int max_n_steps;
    int* goal_coord;
    int* start_coord;
    // vector<int> pool;
    unordered_set<int> drawn_set;
    int* q_rand_coord;
    bool attempting_goal;
    vector< vector<int> > list_brsh;

    std::chrono::time_point<std::chrono::system_clock> t0;
    void print_time(string s){printf("[%.3fs] %s\n",chrono::duration_cast<chrono::duration<double>>(Clock::now() - t0).count(),s.c_str());}

    MyPlanner(double*	_map,
            int _x_size,
            int _y_size,
            double* _armstart_anglesV_rad,
            double* _armgoal_anglesV_rad,
            int _numofDOFs,
            int _planner_id){
      map=_map; x_size=_x_size; y_size=_y_size;
      armstart_anglesV_rad=_armstart_anglesV_rad;
      armgoal_anglesV_rad=_armgoal_anglesV_rad;
      numofDOFs = _numofDOFs;
      planner_id = _planner_id;
      printf("planner_id: %d.\nK: %d.\n",planner_id,K);

      q_rand = new double[numofDOFs];
      q_step = new double[numofDOFs];
      goal_coord = new int[numofDOFs];
      start_coord = new int[numofDOFs];
      q_rand_coord = new int[numofDOFs];
    }

    void plan(double*** plan, int* planlength, int* n_spl){
      *plan = NULL; *planlength = 0;
      t0 = Clock::now();
      switch(planner_id) {
        case RRT:
          rrt_plan(plan, planlength); *n_spl=Tree.size(); break;
        case RRTCONNECT:
          rrtconnect_plan(plan, planlength); *n_spl=Tree.size()+Tree_r.size(); break;
        case RRTSTAR:
          rrtstar_plan(plan, planlength); *n_spl=Tree.size(); break;
        case PRM:
          prm_plan(plan, planlength); *n_spl=roadmap.size(); break;
        case DRRT:
          drrt_plan(plan, planlength); *n_spl=Tree.size(); break;
        default:
          linear_plan(plan, planlength); *n_spl=0;
      }
      for(int i = 0; i < (*planlength); i++) if(!is_valid((*plan)[i])) printf("ERROR: Invalid arm configuration!!!\n");
      print_time("done planning");
    }



    // planners
    int rrt_plan(double*** plan, int* planlength);
    int rrtconnect_plan(double*** plan, int* planlength);
    int rrtstar_plan(double*** plan, int* planlength);
    int prm_plan(double*** plan, int* planlength);
    int _prm_plan(double*** plan, int* planlength);
    int linear_plan(double*** plan, int* planlength);

    // drrt
    int extend();
    void init_res();
    void bresenham();
    bool increase_res();
    bool gen_q_rand_coord();
    bool locate_start_goal();
    int coord2id(int* coord, int res);
    int calc_dist_coord(int* q0, int* q1);
    bool conf2coord(double* conf, int* coord);
    void coord2conf(int* coord, double* conf);
    void id2coord(int id, int* coord, int _res);
    int drrt_plan(double*** plan, int* planlength);
    void drawn_set_insert(int* coord, int res0, int res1);
    bool is_collision_free(double* conf_last, double* conf_new);
    void update_q_near_coord(vector<node> _Tree, int* q_rand_coord);


    // subparts of planners

    int get_q_new(double* q0, double* q1, double** p_q_new, int max_n_steps);
    void update_q_near(vector<node> _Tree, double* q);

    void rewire(vector<node> _Tree);

    void gen_roadmap();
    int astar_find_path();




    // utils

    int is_valid(double* conf){return IsValidArmConfiguration(conf, numofDOFs, map, x_size, y_size);}

    int is_clear(double* q0, double* q1){
      double* q; int flag=get_q_new(q0,q1,&q,INF_STEPS);
      if(flag==REACHED){delete []q; return 1;}
      if(flag==ADVANCED){delete []q;}
      return 0;
    }

    void gen_q_rand(){
      for(int d=0; d<numofDOFs; d++){
        if(d==0) q_rand[d] = (double)rand()/RAND_MAX*PI;
        else q_rand[d] = (double)rand()/RAND_MAX*2*PI;
      }
    }

    void update_q_step(double* q0, double* q1, int n_step){ for(int d=0; d<numofDOFs; d++){q_step[d]=ANG_DIFF(q1[d],q0[d])/(n_step+1);} }

    double calc_dist(double* q0, double* q1){
      double sum=0;
      for(int d=0; d<numofDOFs; d++){sum+=pow(ANG_DIFF(q0[d],q1[d]),2);}
      return sqrt(sum); }

    double get_radius(int n){double r=pow(GAMMA*log(n)/n, 1.0/numofDOFs);return MIN(r,MAX_RADIUS);}

    double get_cost(vector<node> _Tree, int id){double c=0; while(id!=0){c+=_Tree[id].d; id=_Tree[id].p_id;} return c;}

    void update_X_near(vector<node> _Tree, double* q, double radius){ X_near_id.clear(); for(int i=0; i<_Tree.size(); i++){ if(calc_dist(q, _Tree[i].conf)<radius){ X_near_id.push_back(i); } } }

};


#endif
