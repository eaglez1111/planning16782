#include "MyPlanner.h"

int n_sample; // class var
// DRRT

bool MyPlanner::gen_q_rand_coord() {
    cnt++;
    attempting_goal = (cnt%GoalAttemptCnt==0);
    if (attempting_goal) {
        max_n_steps = INF_STEPS;
        for(int i = 0; i < numofDOFs; i++) q_rand_coord[i] = goal_coord[i];
    }
    else {
        max_n_steps = MAX_N_STEPS;
        n_link = RES_MAX / res;
        int rand_range=n_sample-drawn_set.size();
        if(rand_range<=0) return false; // exhausted under current res
        int rand_num = (int)((double)rand()/RAND_MAX*rand_range);
        int i_smp=0, pos=0;
        for(int i=0;i<n_sample;i++){ // i and pos are in the unit of edge
            if(i_smp==rand_num) {pos=i;break;}
            if(!drawn_set.count(i)){i_smp++;}
        }
        id2coord(pos, q_rand_coord, res); // q_rand_coord is in the unit of edge
        for(int i=0;i<numofDOFs;i++){q_rand_coord[i]*=n_link;} // convert q_rand_coord's unit from edge to link(standard)
        coord2conf(q_rand_coord, q_rand);
        if(!is_valid(q_rand)) drawn_set.insert(pos);
    }
    return true;
}

void MyPlanner::id2coord(int id, int* coord, int _res) {
    for(int i= 0; i < numofDOFs; i++) {
        coord[i] = id % _res;
        id /= _res;
    }
}

int MyPlanner::coord2id(int* coord, int res) {
    int id = 0, multiplier = 1;
    for(int i = 0; i < numofDOFs; i++) {
        id += coord[i]*multiplier;
        multiplier *= res;
    }
    return id;
}


bool MyPlanner::increase_res() {
    res *= 2;
    if(res > RES_MAX) return false;
    n_link = RES_MAX / res;
    double* conf_temp;
    int* coord_temp;
    int max_index_last = RES_MAX - n_link * 2;
    for(int i = 1; i < Tree.size(); i++) {
        coord_temp = new int[numofDOFs];
        for(int j = 0; j < numofDOFs; j++) {
            if((Tree[i].conf[j] == 0 && Tree[Tree[i].p_id].conf[j] == max_index_last) || (Tree[i].conf[j] == max_index_last && Tree[Tree[i].p_id].conf[j] == 0))
                coord_temp[j] = max_index_last + n_link;
            else
                coord_temp[j] = min(Tree[i].conf[j], Tree[Tree[i].p_id].conf[j]) + n_link;
        }
        conf_temp = new double[numofDOFs];
        coord2conf(coord_temp, conf_temp);
        Tree.push_back({conf_temp, Tree[i].p_id, coord_temp});
        Tree[i].p_id = Tree.size() - 1;
    }
    // renew drawn_set:
    int set_size=drawn_set.size(), old_res=res/2;
    coord_temp = new int[numofDOFs];
    drawn_set.clear();
    for(int i=0;i<set_size;i++){ // i and coord_temp are in old_res
        id2coord(i,coord_temp,old_res);
        drawn_set_insert(coord_temp, old_res, res);
    }
    n_sample=pow(res, numofDOFs);
    return true;
}

void MyPlanner::drawn_set_insert(int* coord, int res0, int res1){ // coord in res0, drawn_set in res1
    int* coord_temp = new int[numofDOFs];
    for(int d=0;d<numofDOFs;d++){coord_temp[d]=coord[d]*res1/res0;} // drawn_set element is in the unit of edge
    drawn_set.insert(coord2id(coord_temp, res));
    delete[] coord_temp;
}


bool MyPlanner::locate_start_goal() {
    while(true) {
        res *= 2;
        if(res > RES_MAX) return false;
        if(!conf2coord(armstart_anglesV_rad, start_coord)) continue;
        if(!conf2coord(armgoal_anglesV_rad, goal_coord)) continue;
        // initialize tree:
        double* start_conf = new double[numofDOFs];
        coord2conf(start_coord,start_conf);
        Tree.push_back({start_conf,-1,start_coord});
        // initialize drawn_set:
        drawn_set_insert(start_coord, RES_MAX, res); // drawn_set element is in the unit of edge
        return true;
    }
}

bool MyPlanner::conf2coord(double* conf, int* coord) {
    bool found = false;
    for(int i = 0; i < numofDOFs; i++) {
        if (i == 0) coord[i] = (int)(conf[i] / M_PI * res);
        else coord[i] = (int)(conf[i] / (2 * M_PI) * res);
    }
    int* coord_temp = new int[numofDOFs];
    double* conf_temp = new double[numofDOFs];
    int _i;
    for(int i = 0; i < pow(2, numofDOFs); i++) {
        _i = i;
        for(int j = 0; j < numofDOFs; j++) {
            coord_temp[j] = n_link * (coord[j] + _i%2);
            _i /= 2;
        }
        coord2conf(coord_temp, conf_temp);
        if(is_collision_free(conf, conf_temp)) {
            found = true;
            for(int j = 0; j < numofDOFs; j++) coord[j] = coord_temp[j];
            break;
        }
        if(found) break;
    }
    delete[] coord_temp;
    delete[] conf_temp;
    return found;
}

void MyPlanner::coord2conf(int* coord, double* conf) {
    for(int i = 0; i < numofDOFs; i++) {
        if(i == 0) conf[i] = (conf[i] * M_PI) / RES_MAX;
        else conf[i] = (coord[i] * 2 * M_PI) / RES_MAX;
    }
}


int MyPlanner::drrt_plan(double*** plan, int* planlength) {
    if(!locate_start_goal()) return false;
    while(true) {
        if(gen_q_rand_coord()) {
            update_q_near_coord(Tree, q_rand_coord);
            bool flag = extend();
            if(attempting_goal && flag == REACHED) break;
        }
        else {
            if(increase_res() == false) break;
        }
    }
    print_time("Done sampling\n");
    if(res > RES_MAX) {
        printf("Failed!\n");
        return 0;
    }
    else printf("Found!\n");

    vector<double*> _plan;
    int id = Tree.size() - 1;
    while(id != -1) {
        _plan.push_back(Tree[id].conf);
        id = Tree[id].p_id;
    }
    int N = _plan.size();
    *planlength = N;
    *plan = (double**) malloc(N * sizeof(double*));
    for(int i = 0; i < N; i++) {
        (*plan)[i] = _plan[N - 1 - i];
    }
    return 1;
}


bool MyPlanner::is_collision_free(double* conf_last, double* conf_new) {
    double* _spam;
    int flag = get_q_new(conf_last, conf_new, &_spam, max_n_steps);
    if(flag == TRAPPED) return false;
    if(flag == ADVANCED) {
        delete[] _spam;
        return false;
    }
    return true;
}

void MyPlanner::bresenham() {
    list_brsh.clear();
    n_link = RES_MAX / res;
    int* start = new int[numofDOFs];
    int* end = new int[numofDOFs];
    for(int i = 0; i < numofDOFs; i++) {
        start[i] = Tree[q_near_id].coord[i];
        end[i] = q_rand_coord[i];
    }
    int* d = new int[numofDOFs];
    double* point = new double[numofDOFs];
    int N = -INF_STEPS;
    double* dim_step = new double[numofDOFs];
    for(int i = 0; i < numofDOFs; i++) {
        start[i] /= n_link;
        end[i] /= n_link;
        d[i] = end[i] - start[i];
        if(abs(d[i]) > N) N = abs(d[i]);
    }
    for(int i = 0; i < numofDOFs; i++) {
        if(abs(d[i]) > res / 2) {
            if(start[i] < end[i]) start[i] += res;
            else end[i] += res;
        }
        dim_step[i] = d[i] / N;
        point[i] = start[i];
    }
    for(int i = 0; i < N; i++) {
        vector<int> coord;
        for(int j = 0; j < numofDOFs; j++) {
            point[j] += dim_step[j];
            int c = (int)round(point[j]) % res;
            coord.push_back(c * n_link);
        }
        list_brsh.push_back(coord);
    }
    delete[] start;
    delete[] end;
    delete[] d;
    delete[] point;
    delete[] dim_step;
}

int MyPlanner::extend() {
    bresenham();
    double* conf_last = Tree[q_near_id].conf;
    double* conf_new;
    int flag = TRAPPED;
    int p_id = q_near_id;
    for(int i = 0; i < list_brsh.size(); i++) {
        if(i > max_n_steps) return flag;
        conf_new = new double[numofDOFs];
        coord2conf(&list_brsh[i][0], conf_new);
        if(!is_collision_free(conf_last, conf_new)) {
            delete[] conf_new;
            return flag;
        }
        flag = ADVANCED;
        Tree.push_back({conf_new, p_id, &list_brsh[i][0]});
        p_id = Tree.size() - 1;
        conf_last = conf_new;
        // update drawn_set:
        drawn_set_insert(&list_brsh[i][0],RES_MAX,res);
    }
    return REACHED;
}

/* int MyPlanner::get_q_new(double* q0, double* q1, double** p_q_new, int max_n_steps){ */
/*   int n_step = (int)(calc_dist(q0,q1)/step_size); */
/*   update_q_step(q0,q1,n_step); */
/*   (*p_q_new) = new double[numofDOFs]; */
/*   for(int i=1; i<max_n_steps+1; i++){ */
/*     for(int d=0; d<numofDOFs; d++) (*p_q_new)[d]=q0[d]+q_step[d]*i; */
/*     if(is_valid(*p_q_new)){ */
/*       if(i==n_step+1) {return REACHED;} */
/*     } */
/*     else{ */
/*       if(i==1) {delete [](*p_q_new); return TRAPPED;} */
/*       for(int d=0; d<numofDOFs; d++) (*p_q_new)[d]=q0[d]+q_step[d]*(i-1); */
/*       break; */
/*     } */
/*   } */
/*   return ADVANCED; */
/* } */


void MyPlanner::update_q_near_coord(vector<node> _Tree, int* q_rand_coord) {
    int d;
    q_near_id = 0;
    q_new_d = calc_dist_coord(q_rand_coord, _Tree[q_near_id].coord);
    for(int i = 1; i < _Tree.size(); i++) {
        d = calc_dist_coord(q_rand_coord, _Tree[i].coord);
        if(d < q_new_d) {
            q_near_id = i;
            q_new_d = d;
        }
    }
}

int MyPlanner::calc_dist_coord(int* q0, int* q1) {
    int dist = 0;
    for(int i = 0 ; i < numofDOFs; i++) {
        int d = abs(q0[i] = q1[i]);
        if(d > dist) dist = d;
    }
    return dist;
}
