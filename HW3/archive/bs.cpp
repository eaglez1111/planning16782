#include "utils.h"
using namespace std;

using Args = list<string>;
using ArgsPermutation = list<Args>;
using AllArgsPermutation = vector<ArgsPermutation>;

void printPermutations(const AllArgsPermutation& permutations) {
    for (int i=0; i<permutations.size(); ++i) {
        cout << "Permutation[" << i << "]:" << endl;
        for (const auto& args : permutations[i]) {
            for (const auto& s : args) {
                cout << s << " ";
            }
            cout << endl;
        }
    }

}

void DFS(int depth, vector<bool>& visited, Args curr_args, AllArgsPermutation& all_permutations, const vector<string>& symbols) {
    if (depth > symbols.size()) {
        return;
    }

    for (int i=0; i<symbols.size(); ++i) {
        if (visited[i] == false) {
            visited[i] = true;
            curr_args.push_back(symbols[i]);
            all_permutations[depth].push_back(curr_args);

            DFS(depth+1, visited, curr_args, all_permutations, symbols);

            visited[i] = false;
            curr_args.pop_back();
        }
    }
}



AllArgsPermutation getAllPermutation(const unordered_set<string>& symbols) {
    vector<string> symbols_vec;
    for (const auto& s : symbols) {
        symbols_vec.push_back(s);
    }

    AllArgsPermutation result;
    // result.resize(symbols.size());
    for (int i=0; i<symbols.size(); ++i) {
        ArgsPermutation p;
        result.push_back(p);
    }

    int depth = 0;
    vector<bool> visited(symbols.size(), false);
    Args curr_args;

    DFS(depth, visited, curr_args, result, symbols_vec);

    return result;
}


list<GroundedAction> generateAllGroundedActions(const unordered_set<Action, ActionHasher, ActionComparator>& actions, const unordered_set<string>& symbols) {
    const auto& all_permutations = getAllPermutation(symbols);

    // printPermutations(all_permutations);

    list<GroundedAction> all_grounded_actions;
    for (const auto& action : actions) {
        int num_args = action.get_args().size();

        for (const auto& args : all_permutations[num_args-1]) {

            // construct a grounded action based on a specific permutation of arguments
            GroundedAction ga(action.get_name(), args);

            // ground the preconditions and effects of this grounded action
            const auto& preconditions = action.get_preconditions();
            const auto& effects = action.get_effects();
            const auto& arg_names = action.get_args();

            // create args mapping
            unordered_map<string, string> arg_mapping;
            for (const auto& sym : symbols) {
                arg_mapping[sym] = sym;
            }
            auto itr = args.begin();
            for (const auto& arg_name : arg_names) {
                arg_mapping[arg_name] = *itr;
                ++itr;
            }

            // ground preconditions
            for (const auto& pc : preconditions) {
                list<string> mapped_args;
                for (const auto& a : pc.get_args()) {
                    mapped_args.push_back(arg_mapping[a]);
                }
                GroundedCondition gc(pc.get_predicate(), mapped_args, pc.get_truth());
                ga.addGroundedPrecondition(gc);
            }

            // ground effects
            for (const auto& e : effects) {
                list<string> mapped_args;
                for (const auto& a : e.get_args()) {
                    mapped_args.push_back(arg_mapping[a]);
                }
                GroundedCondition ge(e.get_predicate(), mapped_args, e.get_truth());
                ga.addGroundedEffect(ge);
            }

            // cout << ga << endl;
            all_grounded_actions.emplace_back(ga);


        }
    }
    return all_grounded_actions;
}

class State {

public:
    // Constructors
    State() {}
    State(const unordered_set<GroundedCondition, GroundedConditionHasher, GroundedConditionComparator>& cond) {
        setConditions(cond);
    }

    // Setter function
    void setConditions(const unordered_set<GroundedCondition, GroundedConditionHasher, GroundedConditionComparator>& cond) {
        this->conditions = cond;
    }

    bool isValid() {
        return this->valid;
    }

    State branch(const GroundedAction& action) {
        // given an action, branch the next state from the current state
        State new_state(conditions);
        new_state.valid = action.meetPreconditions(conditions);
        if (new_state.valid) {
            // cout << "valid" << endl << endl << endl << endl;
            for (auto effect : action.getEffects()) {
                if (!effect.get_truth()) {
                    // cout << "erased" << effect << endl;
                    effect.set_truth(true);
                    new_state.conditions.erase(effect);
                }
                else {
                    // cout << "inserted" << effect << endl;
                    new_state.conditions.insert(effect);
                }
            }
        }
        return new_state;

    }

    bool operator==(const State& rhs) const {
        if (this->valid != rhs.valid || this->conditions.size() != rhs.conditions.size())
            return false;

        return this->conditions == rhs.conditions;
    }
    string toString() const {
        string temp = "";
        for (const auto& cond : this->conditions) {
            temp += cond.toString();
        }
        return temp;
    }

    // Members
    unordered_set<GroundedCondition, GroundedConditionHasher, GroundedConditionComparator> conditions;
    bool valid;

};

struct StateComparator {
    bool operator()(const State& lhs, const State& rhs) const {
        return lhs == rhs;
    }
};

struct StateHasher
{
    size_t operator()(const State& s) const {
        return hash<string>{}(s.toString());
    }
};

int getHeuristic(const State& s, const State& goal) {
    // number of unsatisfsied conditions in goal state
    int h = goal.conditions.size();
    for (const auto& c1 : goal.conditions) {
        // for each condition in goal state, search for already satisfied ones in start state
        for (const auto& c2 : s.conditions) {
            if (c1 == c2) {
                // a condition is satisfied
                --h;
                break;
            }
        }
    }
    return h;
}

class Node {
public:
    Node() {}
    Node(const State& state, int cost, int heuristic, int id) : state(state), cost(cost), heuristic(heuristic), id(id) {}


    State state;
    int cost;
    int heuristic;

    int id;

};

typedef std::pair<int, int> CostNodeId;
void addToOpenList(const Node& node, std::priority_queue<CostNodeId, std::vector<CostNodeId>, std::greater<CostNodeId> >& open_list) {
    open_list.push(make_pair(node.cost + node.heuristic, node.id));
}

int isVisited(const State& state, const vector<State>& visited_states) {
    for (int i=0; i<visited_states.size(); ++i) {
        if (visited_states[i] == state) {
            return i;
        }
    }
    return -1;
}

vector<int> findPath(int id, const unordered_map<int, int>& parent) {
    vector<int> path;
    int curr_id = id;
    path.push_back(id);
    while (true) {
        curr_id = parent.at(curr_id);
        if (curr_id == 0) {
            break;
        }
        path.push_back(curr_id);
    }
    return path;
}

void printPath(const vector<int>& path, const unordered_map<int, GroundedAction>& actions) {
    cout << "Printing path..." << endl;
    for (int i=path.size()-1; i>=0; --i) {
        // cout << id << endl;
        cout << actions.at(path[i]) << endl;
    }

}

list<GroundedAction> planner(Env* env)
{
    auto t_start = clock();

    // all possible grounded actions
    list<GroundedAction> all_actions = generateAllGroundedActions(env->get_actions(), env->get_symbols());
    // the list of actions to execute to get from start state to goal state
    list<GroundedAction> path;

    // // A* search
    std::priority_queue<CostNodeId, std::vector<CostNodeId>, std::greater<CostNodeId> > open_list;

    unordered_map<int, Node> graph;
    unordered_map<int, GroundedAction> id_to_actions;
    unordered_map<int, int> parent;
    vector<State> visited_states;
    unordered_map<int, bool> closed_list;

    State start(env->get_initial_conditions());
    State goal(env->get_goal_conditions());
    // cout << "Start state: ";
    // for (const auto& cond : start.conditions) {
    //     cout << cond;
    // }
    // cout << endl;

    graph[0] = Node(start, 0, getHeuristic(start, goal), 0);
    parent[0] = -1;
    addToOpenList(graph[0], open_list);
    visited_states.push_back(start);
    // graph[1] = Node(goal, 0, getHeuristic(start, goal), 0);
    int counter = 0;
    while (!open_list.empty()) {
        //         for (const auto& pa : parent) {
        //     cout << pa.first << " to " << pa.second << endl;
        // }
        if(counter%100==0) {cout<<"Expanded "<<counter<<" states"<<endl;}

        // get the node with least cost
        auto current_id = open_list.top().second;
        open_list.pop();
        if (closed_list[current_id] == true)
            continue;
        closed_list[current_id] = true;

        auto& current_node = graph[current_id];
        if (current_node.heuristic == 0) {
            cout << "Goal found!" << endl;
            // back trace to find a path
            const auto& path = findPath(current_id, parent);

            printPath(path, id_to_actions);
            break; // or return path (actions)
        }

        // branch out from current node

        for (const auto& action : all_actions) {
            auto next_state = current_node.state.branch(action);
            if (next_state.isValid()) {

                int visited = isVisited(next_state, visited_states);
                if (visited == -1) {
                    counter++;

                    // not visited, new state
                    visited_states.push_back(next_state);
                    int id = graph.size();
                    Node new_node(next_state, current_node.cost + 1, getHeuristic(next_state, goal), id);
                    graph[id] = new_node;
                    id_to_actions.insert({id, action});
                    parent[id] = current_id;
                    addToOpenList(new_node, open_list);
                } else {
                    // visited
                    if (closed_list[visited] == false) {
                        // not closed yet
                        if (graph[visited].cost > current_node.cost + 1) {
                            // this path has a lower cost
                            graph[visited].cost = current_node.cost + 1;
                            parent[visited] = current_id;
                            addToOpenList(graph[visited], open_list);
                            id_to_actions.at(visited) = action;

                        }
                    }
                }

                // state, cost, heuristic, id


                // cout << "From state: ";
                // for (const auto& cond : current_node.state.conditions) {
                //     cout << cond;
                // }
                // cout << endl;
                //
                // cout << "Applied action: " << action << endl;
                // cout << "With effects: ";
                // for (const auto& eff : action.getEffects()) {
                //     cout << eff;
                // }
                // cout << endl;
                // cout << "To state: ";
                // for (const auto& cond : next_state.conditions) {
                //     cout << cond;
                // }
                // cout << endl << endl;
            }
        }

        // for (const auto& ac : id_to_actions) {
        //     cout << ac.second << endl;
        // }
        // for (const auto& pa : parent) {
        //     cout << pa.first << " to " << pa.second << endl;
        // }

        // loop through all possible actions
        // const auto& new_state = current_node.state.branch(grounded_action)

        // for every new state
        // calculate their heuristics, cost
        // put them into open list





    }


    // blocks world example

    // actions.push_back(GroundedAction("MoveToTable", { "A", "B" }));
    // actions.push_back(GroundedAction("Move", { "C", "Table", "A" }));
    // actions.push_back(GroundedAction("Move", { "B", "Table", "C" }));
    auto t_end = (float)(clock() - t_start) / CLOCKS_PER_SEC;
    cout << "Time taken: " << t_end << " seconds" << endl;

    return path;
}

int main(int argc, char* argv[])
{
    // DO NOT CHANGE THIS FUNCTION
    char* filename = (char*)("example.txt");
    if (argc > 1)
        filename = argv[1];

    cout << "Environment: " << filename << endl << endl;
    Env* env = create_env(filename);
    if (print_status)
    {
        cout << *env;
    }

    list<GroundedAction> actions = planner(env);

    cout << "\nPlan: " << endl;
    for (GroundedAction gac : actions)
    {
        cout << gac << endl;
    }

    return 0;
}
