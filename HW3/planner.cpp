
/* set to 0.0 for no heuristic */
#define A_STAR_WEIGHT 0.0





#include "setup.h"
#include "utils.h"

class MyPlanner {
public:
  list<GroundedAction> path;
  Env* env;
  Status start,goal;
  list<GroundedAction> all_grounded_actions;
  clock_t t0;
  void print_time(string s){ printf("[%.6fs] %s\n", (float)(clock()-t0)/CLOCKS_PER_SEC, s.c_str()); }

  MyPlanner(Env* env):env(env),start(env->initial_conditions),goal(env->goal_conditions),t0(clock()){
    find_all_grounded_actions();
  }

  void find_all_grounded_actions(){
    vector<string> symbols_vec(env->symbols.begin(),env->symbols.end());
    int n_arg_max=0;
    for(const auto& action: env->actions){ n_arg_max=MAX(n_arg_max,action.args.size()); }

    int n_symbol=symbols_vec.size();
    vector<bool> flag(n_symbol);
    vector<int> ind(n_symbol);
    vector<list<list<string>>> all_args(n_arg_max, list<list<string>>());
    for(int k=1; k<=n_arg_max; k++){
      fill(flag.begin(), flag.begin()+k, false);
      fill(flag.end()-k, flag.end(), true);
      do{
        ind.clear();
        for(int i=0; i<n_symbol; i++){ if(flag[i]){ind.push_back(i);}}
        do{
          list<string> args;
          for(const auto& j: ind){ args.push_back(symbols_vec[j]); }
          all_args[k-1].push_back(args);
        } while(next_permutation(ind.begin(),ind.end()));
      } while (next_permutation(flag.begin(), flag.end()));
    }

    for (const auto& action: env->actions) { // Iterate all actions, and each with all possible args combinations
      for (const auto& args: all_args[action.get_args().size()-1]) {
        GroundedAction ga(action.get_name(), args);
        auto itr = args.begin();
        unordered_map<string, string> v2a; // v2a: variable to args
        for (const auto& arg: action.get_args()) {
          v2a[arg] = *itr; itr++;
        }
        assign_status(ga.preconditions, action.get_preconditions(), v2a);
        assign_status(ga.effects, action.get_effects(), v2a);
        all_grounded_actions.emplace_back(ga);
      }
    }
  }


  void assign_status(Status& st, const unordered_set<Condition, ConditionHasher, ConditionComparator>& conditions, unordered_map<string, string>& v2a){
    for(const auto& c: conditions) {
      list<string> _args;
      for(const auto& arg: c.get_args()) { _args.push_back(v2a.count(arg)?v2a[arg]:arg); }
      st.insert(GroundedCondition(c.get_predicate(), _args, c.get_truth()));
    }
  }


  double heu(const Status& st){
    int dist = goal.size();
    for (const auto& ci:goal) {
      for (const auto& cj:st){
        if(ci==cj) { dist--; break; } } }
    return dist;
  }


  bool is_satisfied(const Status& prec, const Status& st) {
      for (const auto& c: prec) { if(!st.count(c)){ return false; } }
      return true;
  }


  void apply_action(Status* p_st, const GroundedAction& ga){
    for (auto effect: ga.effects) {
      if (effect.get_truth()) { (*p_st).insert(effect); }
      else { effect.truth=true; (*p_st).erase(effect); }
    }
  }


  ID st2id(const Status& status){ // status to id
      ID id = "";
      for (const auto& gcd:status){id+=gcd.toString();}
      return id;
  }


  void plan(){
    ID id, idi;
    double Gi,Fi;
    Status* p_sti;

  	set<pair<double, ID>> openlist;
  	openlist.insert(make_pair(0+heu(start), st2id(start)));
  	unordered_map<ID, Node*> NodeList;
    NodeList[st2id(start)] = new Node{&start, NULL, 0.0, false, NULL};

    // Expand
    int cnt = 0;
  	while(!openlist.empty()) {
  		cnt++;
  		id = openlist.begin()->second; // best from openlist
      if(heu(*NodeList[id]->p_st)==0.0) {print_time("goal reached!"); break;} // goal reached
  		openlist.erase(openlist.begin()); // remove from openlist
  		NodeList[id]->closed=true; // add in closedlist

  		for (auto& ga:all_grounded_actions){ // iterate all grounded actions to find all successors
        if(!is_satisfied(ga.preconditions, *NodeList[id]->p_st)){continue;} // action can't be applied
        p_sti = new Status(*NodeList[id]->p_st);
        apply_action(p_sti,ga); idi = st2id(*p_sti); // succesor found

        Gi=NodeList[id]->g+1.0; // new G
        if(!NodeList.count(idi)){ // not visited
          NodeList[idi] = new Node{p_sti, &ga, Gi, false, NodeList[id]};
        }
        else{ // visited
          if(!NodeList[idi]->closed & Gi<NodeList[idi]->g){ // still open & newG better than old
            NodeList[idi]->g = Gi;
            NodeList[idi]->p_ga = &ga;
            NodeList[idi]->parent=NodeList[id];
          }
          else continue; // visited but closed or no need to update
        }
        Fi = A_STAR_WEIGHT==0 ? Gi : Gi+A_STAR_WEIGHT*heu(*p_sti); // (if w==0, don't run heu() at all)
        openlist.insert(make_pair(Fi, idi)); // add to openlist
  		}
  	}
    printf("Expanded %d states. Heuristics weight: %.3f\n", cnt, A_STAR_WEIGHT);

    // Back tracking
    while(NodeList[id]->p_ga!=NULL){
      path.push_front(*NodeList[id]->p_ga);
      id = st2id(*NodeList[id]->parent->p_st);
    }
    print_time("Path found.");
  }
};


list<GroundedAction> planner(Env* env) {MyPlanner mp(env); mp.plan(); return mp.path; }
int main(int argc, char* argv[])
{
    // DO NOT CHANGE THIS FUNCTION
    char* filename = (char*)("example.txt");
    if (argc > 1)
        filename = argv[1];

    cout << "Environment: " << filename << endl << endl;
    Env* env = create_env(filename);
    if (print_status)
    {
        cout << *env;
    }

    list<GroundedAction> actions = planner(env);

    cout << "\nPlan: " << endl;
    for (GroundedAction gac : actions)
    {
        cout << gac << endl;
    }

    return 0;
}
