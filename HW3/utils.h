#ifndef _UTILS_H_
#define _UTILS_H_

#include <time.h>
using namespace std;

typedef unordered_set<GroundedCondition, GroundedConditionHasher, GroundedConditionComparator> Status;
typedef string ID;

#if !defined(MAX)
#define	MAX(A, B)	((A) > (B) ? (A) : (B))
#endif

#if !defined(MIN)
#define	MIN(A, B)	((A) < (B) ? (A) : (B))
#endif

typedef struct _Node{
  Status* p_st; //pointer of statuas
  GroundedAction* p_ga; //pointer of grounded action
  double g;
  bool closed;
  struct _Node* parent;
} Node;








#endif
