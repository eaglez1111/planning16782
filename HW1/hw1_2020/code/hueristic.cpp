#include "hueristic.h"

int dX[NUMOFDIRS+1] = {-1, -1, -1,  0,  0,  1, 1, 1, 0};
int dY[NUMOFDIRS+1] = {-1,  0,  1, -1,  1, -1, 0, 1, 0};

ulli xyt2id(int x, int y, int t){
  return ((ulli)t*XY_MAX+(ulli)x)*XY_MAX+(ulli)y;
}
void id2xyt(ulli id, int &x, int &y, int &t){
  y = id%XY_MAX; id/=XY_MAX;
  x = id%XY_MAX; id/=XY_MAX;
  t = id;
}

void find_path_3d(
        double*	map,
        int collision_thresh,
        int x_size,
        int y_size,
        int robotposeX,
        int robotposeY,
        int target_steps,
        double* target_traj,
        int curr_time,
        double eps,
        int planning_time,
        stack<ulli> &path
      )
{
  printf("planning_time: %d\n", planning_time);
  int x=0, y=0, t=0, xi=0, yi=0, ti=0;
  ulli id0=0,id=0,idi=0;
  double Gi = 0;


  auto start_time = Clock::now();
  printf("clock started, %fs\n",chrono::duration_cast<chrono::duration<double> >(Clock::now() - start_time).count());
  // Find hueristics
  vector<double> hue(x_size*y_size,(numeric_limits<double>::max)());
  calculate_hue(map, collision_thresh, x_size, y_size, target_steps, target_traj, hue);

  printf("hue defined, %fs\n",chrono::duration_cast<chrono::duration<double> >(Clock::now() - start_time).count());
  // Define parameters
  set<F_Id> openlist;
  unordered_set<ulli> closedlist;
  unordered_set<ulli> goals;
  unordered_map<ulli,double> G;
  unordered_map<ulli,double>::iterator G_it;
  unordered_map<ulli,ulli> parent;

  // Construct goal list and open list
  for(int t=0; t<target_steps; t++){
    x = (int)target_traj[t]-1;
    y = (int)target_traj[t+target_steps]-1;
    goals.insert( xyt2id(x,y,t+1) );
  }
  robotposeX-=1; robotposeY-=1;
  id0 = xyt2id(robotposeX,robotposeY,planning_time);
  G[id0]=0;
  openlist.insert( make_pair(0+hue[robotposeY*x_size+robotposeX], id0) );



  printf("before expansion, %fs\n",chrono::duration_cast<chrono::duration<double> >(Clock::now() - start_time).count());
  // Expand
  ulli cnt=0;
  while(!openlist.empty()){ // && cnt<1000000
    cnt++;
    id = (*openlist.begin()).second;
    if(goals.count(id)) {printf("goal reached!\n"); break;} // Goal reached!
    openlist.erase(openlist.begin());
    if(closedlist.count(id)) {continue;} // Already closed, skip expansion
    closedlist.insert(id);

    id2xyt(id,x,y,t);
    ti = t+1;
    for(int i=0;i<NUMOFDIRS+1;i++){
      xi = x+dX[i]; yi = y+dY[i]; // successor's position
      if(map[yi*x_size+xi]>=collision_thresh) {continue;} // is obstacle
      if(xi<0||xi>=x_size||yi<0||yi>=y_size) {continue;} // if out of map
      idi = xyt2id(xi,yi,ti);
      if(closedlist.count(idi)) {continue;} // if already closed

      Gi = G[id] + map[yi*x_size+xi];
      G_it = G.find(idi);
      if(G_it==G.end()) {G[idi] = Gi;}  // not in open list
      else{                             // in open list
        if((G_it->second)>Gi) {G_it->second = Gi;}  // new G value is better, update
        else {continue;}                            // old G value is better, skip
      }
      openlist.insert( make_pair(Gi+eps*hue[yi*x_size+xi], idi) );
      parent[idi] = id;
    }
  }
  printf("%llu expansions!\n", cnt);



  printf("after expansion, %fs\n",chrono::duration_cast<chrono::duration<double> >(Clock::now() - start_time).count());
// Back tracking
  int path_len=0;
  while(id!=id0){
    path.push(id);
    id2xyt(id,x,y,t);
    id = parent[id];
    path_len++;
  }
  printf("%d steps!", path_len);
  printf("done retracking, %fs\n",chrono::duration_cast<chrono::duration<double> >(Clock::now() - start_time).count());


}



void calculate_hue(
        double*	map,
        int collision_thresh,
        int x_size,
        int y_size,
        int target_steps,
        double* target_traj,
        vector<double> &hue
      )
{
  int dX[NUMOFDIRS] = {-1, -1, -1,  0,  0,  1, 1, 1};
  int dY[NUMOFDIRS] = {-1,  0,  1, -1,  1, -1, 0, 1};
  int x=0, y=0, xi=0, yi=0;
  double Gi = 0;

  // Initialize open and closed lists
  set<F_Location> openlist;
  vector<bool> closed(x_size*y_size,false);

  // Insert starting points to openlist
  for(int t=0; t<target_steps; t++){
    x = (int)target_traj[t]-1;
    y = (int)target_traj[t+target_steps]-1;
    hue[y*x_size+x] = 0;  // Starting point have zero g_value
    openlist.insert( make_pair(0.0, make_pair(x,y)) );
  }

  // Expand
  F_Location expanded;
  while (!openlist.empty()) {
    expanded = *openlist.begin();
    openlist.erase(openlist.begin());
    x = expanded.second.first;
    y = expanded.second.second;
    if (closed[y*x_size+x]) {continue;} // Already closed, skip expansion
    closed[y*x_size+x] = true;

    for(int i=0; i<NUMOFDIRS; i++){
      xi = x+dX[i]; yi = y+dY[i]; // successor's position
      if(xi<0||xi>=x_size||yi<0||yi>=y_size) {continue;} // if out of map
      if(closed[yi*x_size+xi]) {continue;} // if already closed
      if(map[yi*x_size+xi]>=collision_thresh) {continue;} // is obstacle
      Gi = hue[y*x_size+x] + map[yi*x_size+xi];
      if(hue[yi*x_size+xi]>Gi) {
        hue[yi*x_size+xi] = Gi;
        openlist.insert( make_pair(Gi, make_pair(xi,yi)) );
      }
    }
  }

}











ulli xy2id(int x, int y){
  return (ulli)x*XY_MAX+(ulli)y;
}
void id2xy(ulli id, int &x, int &y){
  y = id%XY_MAX; id/=XY_MAX;
  x = id%XY_MAX;
}


void find_path_fast(
        double*	map,
        int collision_thresh,
        int x_size,
        int y_size,
        int robotposeX,
        int robotposeY,
        int target_steps,
        double* target_traj,
        int curr_time,
        double eps,
        int planning_time,
        stack<ulli> &path
      )
{
  printf("planning_time: %d\n", planning_time);
  auto start_time = Clock::now();
  printf("clock started, %fs\n",chrono::duration_cast<chrono::duration<double> >(Clock::now() - start_time).count());
  printf("in total %d goals!\n",target_steps);


  int x=0, y=0, t=0, xi=0, yi=0, ti=0;
  ulli id0=0,id=0,idi=0;
  double Gi = 0;
  int goal_found=0;

  // Define parameters
  set<F_Id> openlist;
  unordered_set<ulli> closedlist;
  unordered_set<ulli> goals;
  unordered_map<ulli,double> G;
  unordered_map<ulli,double>::iterator G_it;
  unordered_map<ulli,ulli> parent;

  // Construct goal list and open list
  for(int t=0; t<target_steps; t++){
    x = (int)target_traj[t]-1;
    y = (int)target_traj[t+target_steps]-1;
    goals.insert( xy2id(x,y) );
  }
  robotposeX-=1; robotposeY-=1;
  id0 = xy2id(robotposeX,robotposeY);
  G[id0]=0;
  openlist.insert( make_pair(0, id0) );

  // Expand
  printf("before expansion, %fs\n",chrono::duration_cast<chrono::duration<double> >(Clock::now() - start_time).count());
  ulli cnt=0;
  while(!openlist.empty()&&goal_found<target_steps){
    cnt++;
    id = (*openlist.begin()).second;
    if(goals.count(id)) {goal_found++;} // Goal expanded
    openlist.erase(openlist.begin());
    if(closedlist.count(id)) {continue;} // Already closed, skip expansion
    closedlist.insert(id);

    id2xy(id,x,y);
    for(int i=0;i<NUMOFDIRS;i++){
      xi = x+dX[i]; yi = y+dY[i]; // successor's position
      if(map[yi*x_size+xi]>=collision_thresh) {continue;} // is obstacle
      if(xi<0||xi>=x_size||yi<0||yi>=y_size) {continue;} // if out of map
      idi = xy2id(xi,yi);
      if(closedlist.count(idi)) {continue;} // if already closed

      Gi = G[id] + map[yi*x_size+xi];
      G_it = G.find(idi);
      if(G_it==G.end()) {G[idi] = Gi;}  // not in open list
      else{                             // in open list
        if((G_it->second)>Gi) {G_it->second = Gi;}  // new G value is better, update
        else {continue;}                            // old G value is better, skip
      }
      openlist.insert( make_pair(Gi, idi) );
      parent[idi] = id;
    }
  }
  printf("%llu expansions!\n", cnt);



  printf("after expansion, %fs\n",chrono::duration_cast<chrono::duration<double> >(Clock::now() - start_time).count());
  // Back tracking
  int path_len, waiting_step, waiting_time, best_path_waiting_step, best_goal = 0;
  double waiting_cost, cell_cost, path_cost, best_path_cost=collision_thresh*target_steps;
  bool feasible=1;
  for(int i=0; i<target_steps; i++){
    x = (int)target_traj[i]-1;
    y = (int)target_traj[i+target_steps]-1;
    id=xy2id(x,y);
    path_len=0;
    path_cost=0;
    waiting_cost = collision_thresh;
    feasible=1;
    while(id!=id0){
      path_len++; if(path_len>i-planning_time){feasible=0;break;}
      id2xy(id,x,y); cell_cost = map[y*x_size+x];
      path_cost += cell_cost;
      if(path_cost>=best_path_cost){feasible=0;break;}
      if (cell_cost<waiting_cost){
        waiting_cost = cell_cost;
        waiting_step = path_len;
      }
      id = parent[id];
    }
    if(!feasible) {continue;}
    path_cost += (i-planning_time-path_len)*waiting_cost;
    if(path_cost<best_path_cost){
      best_path_cost = path_cost;
      best_goal = i;
      best_path_waiting_step = waiting_step;
      waiting_time = i-planning_time-path_len;
    }
  }

  printf("done selecting goal, %fs\n",chrono::duration_cast<chrono::duration<double> >(Clock::now() - start_time).count());

  path_len=0;
  x = (int)target_traj[best_goal]-1;
  y = (int)target_traj[best_goal+target_steps]-1;
  id=xy2id(x,y);
  while(id!=id0){
    path.push(id); path_len++;
    if(path_len==best_path_waiting_step){
      path_len+=waiting_time;
      for(int i=0;i<waiting_time;i++){ path.push(id); }
    }
    id = parent[id];
  }
  printf("%d steps!", path_len);
  printf("done retracking, %fs\n",chrono::duration_cast<chrono::duration<double> >(Clock::now() - start_time).count());


}
