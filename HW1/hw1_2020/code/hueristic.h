#ifndef _HUERISTIC_H_
#define _HUERISTIC_H_

#include <unistd.h>
#include <math.h>
#include <mex.h>
#include <iostream>
#include <utility>
#include <vector>
#include <set>
#include <unordered_set>
#include <map>
#include <unordered_map>
#include <stack>

using namespace std;

#include <chrono>
typedef std::chrono::high_resolution_clock Clock;

#define XY_MAX 10000
#define NUMOFDIRS 8
typedef unsigned long long int ulli;
typedef pair<int, int> Location;
typedef pair<double, Location> F_Location;
typedef pair<double, unsigned long long int> F_Id;


/* Input Arguments */
#define	MAP_IN                  prhs[0]
#define	ROBOT_IN                prhs[1]
#define	TARGET_TRAJ             prhs[2]
#define	TARGET_POS              prhs[3]
#define	CURR_TIME               prhs[4]
#define	COLLISION_THRESH        prhs[5]
/* Output Arguments */
#define	ACTION_OUT              plhs[0]
//access to the map is shifted to account for 0-based indexing in the map, whereas
//1-based indexing in matlab (so, robotpose and goalpose are 1-indexed)
#define GETMAPINDEX(X, Y, XSIZE, YSIZE) ((Y-1)*XSIZE + (X-1))
#if !defined(MAX)
#define	MAX(A, B)	((A) > (B) ? (A) : (B))
#endif
#if !defined(MIN)
#define	MIN(A, B)	((A) < (B) ? (A) : (B))
#endif

void calculate_hue(
        double*	map,
        int collision_thresh,
        int x_size,
        int y_size,
        int target_steps,
        double* target_traj,
        vector<double> &hue
      );

void find_path_3d(
        double*	map,
        int collision_thresh,
        int x_size,
        int y_size,
        int robotposeX,
        int robotposeY,
        int target_steps,
        double* target_traj,
        int curr_time,
        double eps,
        int planning_time,
        stack<ulli> &path
      );

void find_path_fast(
        double*	map,
        int collision_thresh,
        int x_size,
        int y_size,
        int robotposeX,
        int robotposeY,
        int target_steps,
        double* target_traj,
        int curr_time,
        double eps,
        int planning_time,
        stack<ulli> &path
      );

ulli xyt2id(int x, int y, int t);
void id2xyt(ulli id, int &x, int &y, int &t);
ulli xy2id(int x, int y);
void id2xy(ulli id, int &x, int &y);

#endif













// ulli st2id(State st);
// State id2st(ulli id);
// ulli next_id(ulli id, int i);

// ulli st2id(State st){
  //   return (st.t*XY_MAX+st.x)*XY_MAX+st.y;
  // }
  // State id2st(ulli id){
    //   State st;
    //   st.y = id%XY_MAX; id /= XY_MAX;
    //   st.x = id%XY_MAX; id /= XY_MAX;
    //   st.t = id;
    //   return st;
    // }
    // ulli next_id(ulli id, int i){
      //   id = id + (1*XY_MAX+dY[i])*XY_MAX+dX[i] ;
      //   return id;
      // }
