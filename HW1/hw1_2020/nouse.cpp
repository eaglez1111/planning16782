#include <iostream>
#include <limits>
#include <math.h>
#include <iostream>
#include <vector>
#include <queue>
#include <set>
#include <unordered_set>
#include <map>
#include <unordered_map>
#include <stack>
#include <mex.h>

using namespace std;

#define NUMOFDIRS 9

/* Input Arguments */
#define	MAP_IN                  prhs[0]
#define	ROBOT_IN                prhs[1]
#define	TARGET_TRAJ             prhs[2]
#define	TARGET_POS              prhs[3]
#define	CURR_TIME               prhs[4]
#define	COLLISION_THRESH        prhs[5]

/* Output Arguments */
#define ACTION_OUT plhs[0]

int first_time=1;
typedef pair<int,int> Location;

struct Cell
{
    double cost_;
    int x_;
    int y_;
    Cell(const double cost, const int x, const int y)
    {
        cost_ = cost;
        x_ = x;
        y_ = y;
    }
    bool operator<(const Cell &val) const { return cost_ > val.cost_; }
};


double *map_;
int collision_thresh_;
int x_size_;
int y_size_;

int target_steps_;
double *target_traj_;
double *g_values;
double *dijkstra_cost_;
int min_cost_point_x_;
int min_cost_point_y_;
int min_cost_vector_index_;
vector<int> dX_{-1, -1, -1, 0, 0, 0, 1, 1, 1};
vector<int> dY_{-1, 0, 1, -1, 0, 1, -1, 0, 1};
double *g_values_;
unordered_map<int,Location> parent;
bool goal_reached_;
vector<Location> path_planned;
int steps_;

vector<Location> getPath(int targetposeX,int targetposeY);
int getPathLength(int targetposeX, int targetposeY);
vector<int> minimumCostPath();

int map_id(int x, int y) {return ((y-1)*x_size_+(x-1));}
bool traversable(int x, int y) {return ( (x>=1&&x<=x_size_&&y>=1&&y<=y_size_) && (map_[map_id(x, y)]<collision_thresh_) );}



static void planner(
        double*	map,
        int collision_thresh,
        int x_size,
        int y_size,
        int robotposeX,
        int robotposeY,
        int target_steps,
        double* target_traj,
        int targetposeX,
        int targetposeY,
        int curr_time,
        double* action_ptr
        )
{
  map_ = map;
  collision_thresh_ = collision_thresh;
  x_size_ = x_size;
  y_size_ = y_size;
  target_steps_ = target_steps;
  target_traj_ = target_traj;
  dijkstra_cost_ = new double[x_size_ * y_size_];

  if (first_time)
  {
    fill_n(dijkstra_cost_, x_size_ * y_size_, INT_MAX);
    priority_queue<Cell> list;
    int src_index = map_id(robotposeX, robotposeY);
    dijkstra_cost_[src_index] = 0.0;
    list.push(Cell(dijkstra_cost_[src_index], robotposeX, robotposeY));
    while (!list.empty())
    {
      Cell current = list.top();
      int current_index = map_id(current.x_, current.y_);
      list.pop();
      for (int i=0;i<dX_.size();i++)
      {
        int neighibor_X =  current.x_+dX_[i];
        int neighibor_Y = current.y_+dY_[i];
        if (traversable(neighibor_X, neighibor_Y))
        {
          int neighibor_index = map_id(neighibor_X, neighibor_Y);
          double cost = dijkstra_cost_[current_index] + map_[neighibor_index];
          if (dijkstra_cost_[neighibor_index] > cost)
          {
            dijkstra_cost_[neighibor_index] = cost;
            list.push(Cell(cost, neighibor_X, neighibor_Y));
            parent[neighibor_index] = make_pair(current.x_, current.y_);
          }
        }
      }
    }

    vector<int> min_cost_trajectory_point = minimumCostPath();

    path_planned = getPath(min_cost_trajectory_point[1], min_cost_trajectory_point[2]);
    for (int i = 0;i<min_cost_trajectory_point[0]-1;i++){
      path_planned.insert(path_planned.begin()+min_cost_trajectory_point[5],make_pair(min_cost_trajectory_point[3],min_cost_trajectory_point[4]));
    }
    steps_ = path_planned.size() - 1;
    first_time = false;
  }

  Location next_step;
  if(steps_>0) {steps_--;}
  action_ptr[0] = path_planned[steps_].first;
  action_ptr[1] = path_planned[steps_].second;
  return;
}




vector<Location> getPath(int targetposeX, int targetposeY)
{
  Location current = make_pair(targetposeX, targetposeY);
  vector<Location> path{current};
  int current_index = map_id(current.first, current.second);
  while (parent.find(current_index) != parent.end()){
    current = parent[current_index];
    current_index = map_id(current.first, current.second);
    path.emplace_back(current);
  }
  return path;
}

int getPathLength(int targetposeX, int targetposeY)
{
  Location current = make_pair(targetposeX, targetposeY);
  vector<Location> path{current};
  int current_index = map_id(current.first, current.second);
  double min_cost = numeric_limits<double>::max();
  int i = 0;
  while (parent.find(current_index) != parent.end()){
    if (min_cost > map_[current_index]) {
      min_cost_vector_index_ = i;
      min_cost_point_x_ = current.first;
      min_cost_point_y_ = current.second;
      min_cost = map_[current_index];
    }
    current = parent[current_index];
    current_index = map_id(current.first, current.second);
    path.emplace_back(current);
    i++;
  }
  return path.size();
}

vector<int> minimumCostPath()
{
  map<double,vector<int>> cost_on_trajectory;
  for (int t = 0; t < target_steps_; t++)
  {
    int goalposeX = (int)target_traj_[t];
    int goalposeY = (int)target_traj_[t + target_steps_];
    int index = map_id(goalposeX, goalposeY);
    int path_length = getPathLength(goalposeX, goalposeY);
    if (t > path_length){
      int wait_time = t-path_length;
      cost_on_trajectory[(wait_time) * map_[map_id(min_cost_point_x_,min_cost_point_y_)] + dijkstra_cost_[index]] = vector<int>{wait_time,goalposeX, goalposeY,min_cost_point_x_,min_cost_point_y_,min_cost_vector_index_};
    }
  }
  return cost_on_trajectory[cost_on_trajectory.begin()->first];
}

void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])

{

        /* Check for proper number of arguments */
        if (nrhs != 6)
        {
                mexErrMsgIdAndTxt("MATLAB:planner:invalidNumInputs",
                                  "Six input arguments required.");
        }
        else if (nlhs != 1)
        {
                mexErrMsgIdAndTxt("MATLAB:planner:maxlhs",
                                  "One output argument required.");
        }

        /* get the dimensions of the map and the map matrix itself*/
        int x_size = mxGetM(MAP_IN);
        int y_size = mxGetN(MAP_IN);
        double *map = mxGetPr(MAP_IN);

        /* get the dimensions of the robotpose and the robotpose itself*/
        int robotpose_M = mxGetM(ROBOT_IN);
        int robotpose_N = mxGetN(ROBOT_IN);
        if (robotpose_M != 1 || robotpose_N != 2)
        {
                mexErrMsgIdAndTxt("MATLAB:planner:invalidrobotpose",
                                  "robotpose vector should be 1 by 2.");
        }
        double *robotposeV = mxGetPr(ROBOT_IN);
        int robotposeX = (int)robotposeV[0];
        int robotposeY = (int)robotposeV[1];

        /* get the dimensions of the goalpose and the goalpose itself*/
        int targettraj_M = mxGetM(TARGET_TRAJ);
        int targettraj_N = mxGetN(TARGET_TRAJ);

        if (targettraj_M < 1 || targettraj_N != 2)
        {
                mexErrMsgIdAndTxt("MATLAB:planner:invalidtargettraj",
                                  "targettraj vector should be M by 2.");
        }
        double *targettrajV = mxGetPr(TARGET_TRAJ);
        int target_steps = targettraj_M;

        /* get the current position of the target*/
        int targetpose_M = mxGetM(TARGET_POS);
        int targetpose_N = mxGetN(TARGET_POS);
        if (targetpose_M != 1 || targetpose_N != 2)
        {
                mexErrMsgIdAndTxt("MATLAB:planner:invalidtargetpose",
                                  "targetpose vector should be 1 by 2.");
        }
        double *targetposeV = mxGetPr(TARGET_POS);
        int targetposeX = (int)targetposeV[0];
        int targetposeY = (int)targetposeV[1];

        /* get the current timestep the target is at*/
        int curr_time = mxGetScalar(CURR_TIME);

        /* Create a matrix for the return action */
        ACTION_OUT = mxCreateNumericMatrix((mwSize)1, (mwSize)2, mxDOUBLE_CLASS, mxREAL);
        double *action_ptr = (double *)mxGetData(ACTION_OUT);

        /* Get collision threshold for problem */
        int collision_thresh = (int)mxGetScalar(COLLISION_THRESH);

        planner(map, collision_thresh, x_size, y_size, robotposeX, robotposeY, target_steps, targettrajV, targetposeX, targetposeY, curr_time, &action_ptr[0]);

        // Planner plan(map, collision_thresh, x_size, y_size, target_steps, targettrajV);
        // plan.execute(robotposeX, robotposeY, targetposeX, targetposeY, curr_time, action_ptr);
        //     printf("DONE PLANNING!\n");
        return;
}
