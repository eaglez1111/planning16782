#ifndef __LINKEDLIST_H
#define __LINKEDLIST_H

#include <stdio.h>
#include <stdlib.h>

typedef struct _State{
  int x,y;
  int g;
} State;


typedef State dataType;
static char isEqual(dataType A,dataType B){
  return ((A.x==B.x)&&(A.y==B.y))?1:0;
}
static char* stringlize(dataType d){
  static char st[40];
  sprintf(st,"(%4d,%4d) g=%5d",d.x,d.y,d.g);
  return st;
}
static char smallerThan(dataType A,dataType B){  //return 1 if A<B
  return (A.g<B.g)?1:0;
}
/*
typedef int dataType;
static char isEqual(dataType A,dataType B){
  return (A==B)?1:0;
}
static char* stringlize(dataType d){
  static char st[40];
  sprintf(st,"%d",d);
  return st;
}
static char smallerThan(dataType A,dataType B){  //return 1 if A<B
  return (A<B)?1:0;
}
*/

typedef struct _Node{
  dataType data;
  struct _Node* pNext;
} Node;

Node* initList();
int getLength(Node* list);
int printList(Node* list);

Node* getPNode(Node* list, int index);
int getIndex(Node* list, dataType d);
Node* getPrecedingPNode(Node* list, dataType d);

char deleteIndex(Node* L, int index);
char deleteData(Node* list, dataType d);
char deleteFollowingNode(Node* list, Node* p);
char insert(Node* list, dataType d, int index);

char sort(Node* list); //Undefined
Node* getPNodeBeforeMinOrMax(Node* list, char minWanted);
static char isEmpty(Node* list){ return !(list->pNext); }

char freeList(Node* list);


#endif
