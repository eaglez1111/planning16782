#include"linkedList.h"

Node* initList(){
  Node* list = 0;
  list = (Node*)malloc(sizeof(Node));
  if(list==0){ printf("Memory allocation failed in creatList()"); }
  else{ list->pNext=0; }
  return list;
}

int getLength(Node* list){
  Node* p = list;
  int i=0;
  while(p->pNext!=0){
    i++;
    p=p->pNext;
  }
}

int printList(Node* list){
  Node* p = list;
  int i=0;
  while(p->pNext!=0){
    p=p->pNext;
    printf("%d - %s\n", i,stringlize(p->data));
    i++;
  }
  return 1;
}

Node* getPNode(Node* list, int index){ // index(-1) refers to list itself, index(0) refers to the first node
  Node* p=list;
  for(int i=-1;i<index;i++){
    p=p->pNext;
    if(p==0){ return 0; break; }  // return 0 when list_length < index
  }
  return p;
}

int getIndex(Node* list, dataType d){
  Node* p = list;
  int index=-1;
  while(p->pNext!=0){
    p=p->pNext;
    index++;
    if(isEqual(d,p->data)){ return index; }
  }
  return -1;
}

Node* getPrecedingPNode(Node* list, dataType d){
  Node *procedingP=0, *p = list;
  while(p->pNext!=0){
    if(isEqual(d,p->pNext->data)){ procedingP=p; break; }
    p=p->pNext;
  }
  return(procedingP);
}

char deleteIndex(Node* list, int index){
  Node* pNodeBeforeIndex = getPNode(list,index-1);
  return deleteFollowingNode(list,pNodeBeforeIndex);
}

char deleteData(Node* list, dataType d){
  Node* pNodeBeforeIndex = getPrecedingPNode(list,d);
  return deleteFollowingNode(list,pNodeBeforeIndex);
}

char deleteFollowingNode(Node* list, Node* p){
  if(p==0){ return 0; }
  else if(p->pNext==0){ return 0; }
  else{
    Node* pNodeToBeFreed=0;
    pNodeToBeFreed=p->pNext;
    p->pNext=pNodeToBeFreed->pNext;
    free(pNodeToBeFreed);
  }
  return 1;
}

char insert(Node* list, dataType d, int index){
  Node* p=0;
  Node* pNodeBeforeIndex = getPNode(list,index-1);
  if(pNodeBeforeIndex==0){ return 0; }
  else{
    p = (Node*)malloc(sizeof(Node));
    if(p==0){
      printf("Memory allocation failed in insert()");
      return 0;
    }
    else{
      p->data=d;
      p->pNext = pNodeBeforeIndex->pNext;
      pNodeBeforeIndex->pNext = p;
    }
  }
  return 1;
}

Node* getPNodeBeforeMinOrMax(Node* list, char minWanted){
  if(!isEmpty(list)){
    Node *p=list, *result=list;
    while(p->pNext!=0){
      if (
        (minWanted && !smallerThan(result->pNext->data,p->pNext->data)) ||
        (!minWanted && !smallerThan(p->pNext->data,result->pNext->data))
      ) {
        result=p;
      }
      p=p->pNext;
    }
    return result;
  }
  return 0;
}

char freeList(Node* list){
  while(list->pNext!=0){
    deleteFollowingNode(list,list);
  }
  //free(list);
  return 1;
}


//
