#ifndef __ASTAR_H
#define __ASTAR_H

#include <stdio.h>
#include <stdlib.h>
#include "linkedList.h"

#define INT_MAX_EZ 2147483647
#define NUMOFDIRS 8
static int dX[NUMOFDIRS] = {-1, -1, -1,  0,  0,  1, 1, 1}; // 8-connected grid
static int dY[NUMOFDIRS] = {-1,  0,  1, -1,  1, -1, 0, 1};

//access to the map is shifted to account for 0-based indexing in the map, whereas
//1-based indexing in matlab (so, robotpose and goalpose are 1-indexed)
#define GETMAPINDEX(X, Y, XSIZE, YSIZE) ((Y-1)*XSIZE + (X-1))
//#define GETMAPINDEX(X, Y, XSIZE, YSIZE) ((Y)*XSIZE + (X))


int findGoal(int target_steps, int curr_time, char foundPath);
int heu(State s1, State s2);
char findPath(double* map, int collision_thresh, int x_size, int y_size, State sStart, State sGoal, int steps, Node* listPath);

char f(int *p);

#endif
