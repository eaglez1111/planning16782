#include "aStar.h"

int findGoal(int target_steps, int curr_time, char foundPath){
  static int goal=0, failedGoal=0, securedGoal=-1;
  if(securedGoal==-1) securedGoal=(target_steps-1)*2;
  if(curr_time>failedGoal){
    failedGoal=curr_time;
  }
  if(foundPath){
    securedGoal=goal;
  }
  else{
    failedGoal=goal;
  }
  goal=(int)((failedGoal+securedGoal)/2);
  return goal;
}

int heu(State s1, State s2){
  int dx = s1.x-s2.x;
  int dy = s1.y-s2.y;
  dx = dx>=0?dx:-dx;
  dy = dy>=0?dy:-dy;
  return (dx>dy)?dx:dy;
}
