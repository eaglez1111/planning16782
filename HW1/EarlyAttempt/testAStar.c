#include "aStar.h"

int main(int argc,char **argv){

  int map[]={0,6,0,0,
             0,2,2,0,
             2,0,4,0,
             0,0,1,0};
  State sStart,sGoal;
  sStart.x=0; sStart.y=3; sStart.g=0;
  sGoal.x=2; sGoal.y=0; sGoal.g=9996;
  Node* listPath=initList();
  printf("%d\n",findPath(map,10,4,4,sStart,sGoal,listPath));
  printList(listPath);
  return 0;
}

/*
int main(int argc,char **argv){

  int map[]={0,6,0,0,0,0,
             0,0,6,0,4,4,
             1,5,4,1,2,0,
             0,0,7,9,3,0,
             0,0,0,0,7,0,
             0,0,9,0,0,0};
  State sStart,sGoal;
  sStart.x=1; sStart.y=5; sStart.g=0;
  sGoal.x=5; sGoal.y=0; sGoal.g=9998;
  Node* listPath=initList();
  printf("%d\n",findPath(map,3,6,6,sStart,sGoal,listPath));
  printList(listPath);


  return 0;
}
*/
