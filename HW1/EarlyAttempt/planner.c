/*=================================================================
 *
 * planner.c
 *
 *=================================================================*/
#include <math.h>
#include "mex.h"
#include "linkedList.h"
#include "aStar.h"

/* Input Arguments */
#define	MAP_IN                  prhs[0]
#define	ROBOT_IN                prhs[1]
#define	TARGET_TRAJ             prhs[2]
#define	TARGET_POS              prhs[3]
#define	CURR_TIME               prhs[4]
#define	COLLISION_THRESH        prhs[5]


/* Output Arguments */
#define	ACTION_OUT              plhs[0]


#if !defined(MAX)
#define	MAX(A, B)	((A) > (B) ? (A) : (B))
#endif

#if !defined(MIN)
#define	MIN(A, B)	((A) < (B) ? (A) : (B))
#endif



char findPath(double* map, int collision_thresh, int x_size, int y_size, State sStart, State sGoal, int steps, Node* listPath){
  printf("thres%d",collision_thresh);
  int cnt_ez=0;
  printf("!!");freeList(listPath);
  if(listPath==0){ printf("ez0"); return 0; }
  insert(listPath,sGoal,0);
  int listPathLen=0;
  Node *listOpen=initList(), *listClosed=initList();
  insert(listOpen,sStart,0);
  /* Computes g-values for "relevant" states */
  while(1){
    Node *p=listOpen, *procedingP=listOpen; // procedingP is the pNode before the min of listOpen

    /* Find the node to expand */
    while(p->pNext!=0){  // Find the min g+h
      //printf("%d,%d:%d+3*%d < %d+3*%d\n",p->pNext->data.x,p->pNext->data.y,p->pNext->data.g,heu(p->pNext->data,sGoal),procedingP->pNext->data.g,heu(procedingP->pNext->data,sGoal) );
      if( p->pNext->data.g+3*heu(p->pNext->data,sGoal) < procedingP->pNext->data.g+3*heu(procedingP->pNext->data,sGoal) ){
        procedingP=p;
      }
      p=p->pNext;
    }

    /* Move NodeToBeExpanded to closed list */
    //cnt_ez++; if(cnt_ez>10) {return 0;}
    insert(listClosed,procedingP->pNext->data,0);
    deleteFollowingNode(listOpen,procedingP);
    procedingP=listClosed;

    /* Reached goal */
    if(isEqual(procedingP->pNext->data,sGoal)) { break; }

    printf("ex:(%d,%d)-%d\n",procedingP->pNext->data.x,procedingP->pNext->data.y,procedingP->pNext->data.g);
    //printList(listOpen);
    /* Expand to successors */
    State nextS;  // successor after the state being expanded
    for(int i=0;i<NUMOFDIRS;i++){
      nextS.x = procedingP->pNext->data.x + dX[i];
      nextS.y = procedingP->pNext->data.y + dY[i];
      //printf("ep0\n");
      if(nextS.x>=x_size||nextS.x<=0||nextS.y>=y_size||nextS.y<=0){ continue; } //exceed the map physically
      //printf("ep1 %d>=%d?\n",newGProposedForNextS,collision_thresh);
      if((int)map[GETMAPINDEX(nextS.x,nextS.y,x_size,y_size)]>=collision_thresh){ continue; }
      int newGProposedForNextS = procedingP->pNext->data.g + 1;//(int)map[GETMAPINDEX(nextS.x,nextS.y,x_size,y_size)];
      //printf("ep2\n");
      if(getPrecedingPNode(listClosed,nextS)!=0){ continue; } // already visited and closed

      Node* pNodeBeforeNextS = getPrecedingPNode(listOpen,nextS);
      //printf("ep3\n");
      if(pNodeBeforeNextS!=0){  // nextS is already in listOpen
        if ( pNodeBeforeNextS->pNext->data.g <= newGProposedForNextS ){ continue; }
      }
      else{ // nextS isn't in listOpen
        //printf("inserted:%d,%d",nextS.x,nextS.y);
        insert(listOpen,nextS,0);
        pNodeBeforeNextS=listOpen;
      }
    pNodeBeforeNextS->pNext->data.g = newGProposedForNextS; // update g-value
    }

    //printf("listOpen:\n");
    //printList(listOpen);

    if(isEmpty(listOpen)) { //Running out of states to explore before visiting sGoal --Goal isn't reachable
      freeList(listOpen); free(listOpen);
      freeList(listClosed); free(listClosed);
      printf("ez1"); return 0;
    }
  }
  //printf("done expanding\n");


  freeList(listOpen); free(listOpen); // Release memory assigned to listOpen




  /* Find the least cost path (Greedy Search) */
  if(!isEqual(listClosed->pNext->data,sGoal)){  //should never happen
    freeList(listClosed); free(listClosed);
    printf("ez2"); return 0;
  }
  else {
    //insert(listPath,listClosed->pNext->data,0);
    deleteFollowingNode(listClosed,listClosed);
  } //take Goal out of listClosed
  State currentState=sGoal;
  //printf("starting greedy\n");
  while(!isEqual(currentState,sStart)){
    /* find min from 8 states leading to current state */
    Node* pNodeBeforeMinG=0; // in listClosed
    int minG=INT_MAX_EZ;
    State nextS;  // successor after the expanded node

    //printf("from %d,%d-%d\n",currentState.x,currentState.y,currentState.g);
    for(int i=0;i<NUMOFDIRS;i++){
      nextS.x = currentState.x + dX[i];
      nextS.y = currentState.y + dY[i];
      if(nextS.x>x_size||nextS.x<0||nextS.y>y_size||nextS.y<0){ continue; } //exceed the map physically
      Node* pNodeBeforeNextS = getPrecedingPNode(listClosed,nextS);
      if(pNodeBeforeNextS==0){ continue; } // this nextS doesn't have a g-value
      if( (pNodeBeforeNextS->pNext->data.g<minG)||(pNodeBeforeNextS->pNext->data.g==minG&&heu(sStart,pNodeBeforeNextS->pNext->data)<heu(sStart,pNodeBeforeMinG->pNext->data)) ){
        minG=pNodeBeforeNextS->pNext->data.g;
        pNodeBeforeMinG=pNodeBeforeNextS;
      }
    }
    //printf("got  %d,%d-%d\n",pNodeBeforeMinG->pNext->data.x,pNodeBeforeMinG->pNext->data.y,pNodeBeforeMinG->pNext->data.g);
    if(pNodeBeforeMinG==0){ // Should never happen, because only sStart doesn't have any state leading to it and sStart wouldn't enter loop this time
      freeList(listClosed); free(listClosed);
      printf("ez3"); return 0;
    }
    insert(listPath,pNodeBeforeMinG->pNext->data,0);
    listPathLen++;
    currentState=pNodeBeforeMinG->pNext->data;
    deleteFollowingNode(listClosed,pNodeBeforeMinG);
    if(pNodeBeforeMinG->pNext==0){ break; }
  }
  //printf("done greedy\n");
  /* Return path */
  if(!isEqual(listPath->pNext->data,sStart)){  // Should never happen
    freeList(listClosed); free(listClosed);
    printf("ez4");return 0;
  }
  freeList(listClosed); free(listClosed);
  printf("steps:%d len:%d\n",steps,listPathLen);
  if(listPathLen>steps) {printf("ez5-0");return 0;}
  else {printf("ez5-1");return 1;}
}







static void planner(
        double*	map,
        int collision_thresh,
        int x_size,
        int y_size,
        int robotposeX,
        int robotposeY,
        int target_steps,
        double* target_traj,
        int targetposeX,
        int targetposeY,
        int curr_time,
        double* action_ptr
        )
{
  printf("curr_time:%d\n",curr_time);

  static int lastGoal=0,goal=0;
  static char foundPath=0;
  static Node *listPath, *lastListPath;
  static char flag_listCreated=0;
  if(!flag_listCreated){flag_listCreated=1;listPath=initList();}

  //if(foundPath==0)
  {
    lastGoal=goal;
    goal=findGoal(target_steps,curr_time,foundPath);
    printf("\n\ngoal=%d\n",goal);
    if(goal>=target_steps){ // No solution, can never catch the target
      printf("ez6\n");
    }
    else if(lastGoal==goal){ // Attempt to findPath for the same goal, meaning the most optimized goal is found
      printf("ez7\n");
    }
    else{
      printf("ez8\n");
      State sStart; sStart.x=robotposeX; sStart.y=robotposeY; sStart.g=0;
      State sGoal; sGoal.x=(int)target_traj[goal]; sGoal.y=(int)target_traj[goal+target_steps]; sGoal.g=INT_MAX_EZ;
      printf("(%3d,%3d)>>(%3d,%3d)\n", sStart.x, sStart.y, sGoal.x, sGoal.y);
      foundPath=0;
      foundPath=findPath(map,collision_thresh,x_size,y_size, sStart,sGoal,goal-curr_time,listPath);
      if(foundPath){
        printList(listPath);

        if(lastListPath!=0){freeList(lastListPath);free(lastListPath);}
        lastListPath=listPath;
        deleteFollowingNode(lastListPath,lastListPath);
        listPath=initList();
      }
    }
  }

  //deleteFollowingNode(listPath,listPath);
  if(lastListPath==0||lastListPath->pNext==0){
    action_ptr[0] = robotposeX;
    action_ptr[1] = robotposeY;
  }
  else{
    action_ptr[0] = (int)lastListPath->pNext->data.x;// robotposeX+1;//
    action_ptr[1] = (int)lastListPath->pNext->data.y;// robotposeY+1;//
    deleteFollowingNode(lastListPath,lastListPath);
  }
  printf("%d,%d\n",targetposeX,targetposeY);
  printf("%d,%d--%d,%d\n",robotposeX,robotposeY,(int)action_ptr[0],(int)action_ptr[1]);
  return;
}


// prhs contains input parameters (4):
// 1st is matrix with all the obstacles
// 2nd is a row vector <x,y> for the robot position
// 3rd is a matrix with the target trajectory
// 4th is an integer C, the collision threshold for the map
// plhs should contain output parameters (1):
// 1st is a row vector <dx,dy> which corresponds to the action that the robot should make
void mexFunction( int nlhs, mxArray *plhs[],
        int nrhs, const mxArray*prhs[] )

{

    /* Check for proper number of arguments */
    if (nrhs != 6) {
        mexErrMsgIdAndTxt( "MATLAB:planner:invalidNumInputs",
                "Six input arguments required.");
    } else if (nlhs != 1) {
        mexErrMsgIdAndTxt( "MATLAB:planner:maxlhs",
                "One output argument required.");
    }

    /* get the dimensions of the map and the map matrix itself*/
    int x_size = mxGetM(MAP_IN);
    int y_size = mxGetN(MAP_IN);
    double* map = mxGetPr(MAP_IN);

    /* get the dimensions of the robotpose and the robotpose itself*/
    int robotpose_M = mxGetM(ROBOT_IN);
    int robotpose_N = mxGetN(ROBOT_IN);
    if(robotpose_M != 1 || robotpose_N != 2){
        mexErrMsgIdAndTxt( "MATLAB:planner:invalidrobotpose",
                "robotpose vector should be 1 by 2.");
    }
    double* robotposeV = mxGetPr(ROBOT_IN);
    int robotposeX = (int)robotposeV[0];
    int robotposeY = (int)robotposeV[1];

    /* get the dimensions of the goalpose and the goalpose itself*/
    int targettraj_M = mxGetM(TARGET_TRAJ);
    int targettraj_N = mxGetN(TARGET_TRAJ);

    if(targettraj_M < 1 || targettraj_N != 2)
    {
        mexErrMsgIdAndTxt( "MATLAB:planner:invalidtargettraj",
                "targettraj vector should be M by 2.");
    }
    double* targettrajV = mxGetPr(TARGET_TRAJ);
    int target_steps = targettraj_M;

    /* get the current position of the target*/
    int targetpose_M = mxGetM(TARGET_POS);
    int targetpose_N = mxGetN(TARGET_POS);
    if(targetpose_M != 1 || targetpose_N != 2){
        mexErrMsgIdAndTxt( "MATLAB:planner:invalidtargetpose",
                "targetpose vector should be 1 by 2.");
    }
    double* targetposeV = mxGetPr(TARGET_POS);
    int targetposeX = (int)targetposeV[0];
    int targetposeY = (int)targetposeV[1];

    /* get the current timestep the target is at*/
    int curr_time = mxGetScalar(CURR_TIME);

    /* Create a matrix for the return action */
    ACTION_OUT = mxCreateNumericMatrix( (mwSize)1, (mwSize)2, mxDOUBLE_CLASS, mxREAL);
    double* action_ptr = (double*) mxGetData(ACTION_OUT);

    /* Get collision threshold for problem */
    int collision_thresh = (int) mxGetScalar(COLLISION_THRESH);

    /* Do the actual planning in a subroutine */
    planner(map, collision_thresh, x_size, y_size, robotposeX, robotposeY, target_steps, targettrajV, targetposeX, targetposeY, curr_time, &action_ptr[0]);
    // printf("DONE PLANNING!\n");
    return;
}
