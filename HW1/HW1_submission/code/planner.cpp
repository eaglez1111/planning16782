/* To TA: */

//  for map 1-4, this should be 0, so to not use 3D search;
//  for other maps, setting 0 and setting 1 are two solutions.
#define USE_3D_SEARCH 0

/* * * * */








#include "hueristic.h"

stack<ulli> path;
int last_curr_time=9999;
float eps=0;
int planning_time=0; // leave enough time to be safe
int x,y,t;

static void planner(
        double*	map,
        int collision_thresh,
        int x_size,
        int y_size,
        int robotposeX,
        int robotposeY,
        int target_steps,
        double* target_traj,
        int targetposeX,
        int targetposeY,
        int curr_time,
        double* action_ptr
        )
{
    printf("curr_time:%d \n",curr_time);
    if(last_curr_time>=curr_time){ // new map/task
      while(!path.empty()){path.pop();} //empty the stack
      if(USE_3D_SEARCH){
        switch(target_steps) {
          case 5345: planning_time=50; break; // map1
          case 5245: planning_time=80; break; // map2
          case 792: planning_time=5; break; // map3&4
        }
        find_path_3d(map, collision_thresh, x_size, y_size, robotposeX, robotposeY,
                  target_steps, target_traj, curr_time, eps, planning_time, path);
      }
      else{
        switch(target_steps) {
          case 5345: planning_time=3; break; // map1
          case 5245: planning_time=5; break; // map2
          case 792: planning_time=0; break; // map3&4
        }
        find_path_fast(map, collision_thresh, x_size, y_size, robotposeX, robotposeY,
          target_steps, target_traj, curr_time, eps, planning_time, path);
      }
    }


    if(!path.empty() && curr_time>=planning_time){
      id2xyt(path.top(),x,y,t);
      // printf("%d,%d : %d,%d,%d - %d,%d,%d \n",robotposeX,robotposeY, x,y,t,targetposeX-1,targetposeY-1,curr_time);
      action_ptr[0] = 1+x; // account for zero-index
      action_ptr[1] = 1+y; // account for zero-index
      path.pop();
    }
    else {
      // printf("xxx,xxx,0 -- %d,%d,%d \n",targetposeX-1,targetposeY-1,curr_time);
      action_ptr[0] = robotposeX;
      action_ptr[1] = robotposeY;
    }
    last_curr_time = curr_time;
    return;
}





















// prhs contains input parameters (4):
// 1st is matrix with all the obstacles
// 2nd is a row vector <x,y> for the robot position
// 3rd is a matrix with the target trajectory
// 4th is an integer C, the collision threshold for the map
// plhs should contain output parameters (1):
// 1st is a row vector <dx,dy> which corresponds to the action that the robot should make
void mexFunction( int nlhs, mxArray *plhs[],
        int nrhs, const mxArray*prhs[] )

{

    /* Check for proper number of arguments */
    if (nrhs != 6) {
        mexErrMsgIdAndTxt( "MATLAB:planner:invalidNumInputs",
                "Six input arguments required.");
    } else if (nlhs != 1) {
        mexErrMsgIdAndTxt( "MATLAB:planner:maxlhs",
                "One output argument required.");
    }

    /* get the dimensions of the map and the map matrix itself*/
    int x_size = mxGetM(MAP_IN);
    int y_size = mxGetN(MAP_IN);
    double* map = mxGetPr(MAP_IN);

    /* get the dimensions of the robotpose and the robotpose itself*/
    int robotpose_M = mxGetM(ROBOT_IN);
    int robotpose_N = mxGetN(ROBOT_IN);
    if(robotpose_M != 1 || robotpose_N != 2){
        mexErrMsgIdAndTxt( "MATLAB:planner:invalidrobotpose",
                "robotpose vector should be 1 by 2.");
    }
    double* robotposeV = mxGetPr(ROBOT_IN);
    int robotposeX = (int)robotposeV[0];
    int robotposeY = (int)robotposeV[1];

    /* get the dimensions of the goalpose and the goalpose itself*/
    int targettraj_M = mxGetM(TARGET_TRAJ);
    int targettraj_N = mxGetN(TARGET_TRAJ);

    if(targettraj_M < 1 || targettraj_N != 2)
    {
        mexErrMsgIdAndTxt( "MATLAB:planner:invalidtargettraj",
                "targettraj vector should be M by 2.");
    }
    double* targettrajV = mxGetPr(TARGET_TRAJ);
    int target_steps = targettraj_M;

    /* get the current position of the target*/
    int targetpose_M = mxGetM(TARGET_POS);
    int targetpose_N = mxGetN(TARGET_POS);
    if(targetpose_M != 1 || targetpose_N != 2){
        mexErrMsgIdAndTxt( "MATLAB:planner:invalidtargetpose",
                "targetpose vector should be 1 by 2.");
    }
    double* targetposeV = mxGetPr(TARGET_POS);
    int targetposeX = (int)targetposeV[0];
    int targetposeY = (int)targetposeV[1];

    /* get the current timestep the target is at*/
    int curr_time = mxGetScalar(CURR_TIME);

    /* Create a matrix for the return action */
    ACTION_OUT = mxCreateNumericMatrix( (mwSize)1, (mwSize)2, mxDOUBLE_CLASS, mxREAL);
    double* action_ptr = (double*) mxGetData(ACTION_OUT);

    /* Get collision threshold for problem */
    int collision_thresh = (int) mxGetScalar(COLLISION_THRESH);

    /* Do the actual planning in a subroutine */
    planner(map, collision_thresh, x_size, y_size, robotposeX, robotposeY, target_steps, targettrajV, targetposeX, targetposeY, curr_time, &action_ptr[0]);
    // printf("DONE PLANNING!\n");
    return;
}
