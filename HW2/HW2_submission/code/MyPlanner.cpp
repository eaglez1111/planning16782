#include "MyPlanner.h"


// RRT

int MyPlanner::rrt_plan(double*** plan, int* planlength) {
  int i;
  double* q_new;
  int flag=TRAPPED;
  int max_n_steps=MAX_N_STEPS;

  // init
  Tree.push_back({armstart_anglesV_rad,-1});

  // sample and build tree
  for(i=0;i<K;i++){
    if(i%GoalAttemptCnt==0){
      for(int d=0; d<numofDOFs; d++) q_rand[d]=armgoal_anglesV_rad[d];
      max_n_steps = INF_STEPS;
    }
    else{
      gen_q_rand();
      max_n_steps = MAX_N_STEPS;
    }
    update_q_near(Tree, q_rand);
    flag = get_q_new(Tree[q_near_id].conf, q_rand, &q_new, max_n_steps);
    if(flag!=TRAPPED) {Tree.push_back({q_new,q_near_id});}
    if((i%GoalAttemptCnt==0)&&(flag==REACHED)) {break;}
  }
  print_time("done sampling");
  if (i==K) { printf("Failed!\n"); return 0; }
  else { printf("Found! w/ %d samples.\n",i);}

  // output and format the plan
  vector<double*> _plan;
  int id = Tree.size()-1;
  while(id!=-1){
    _plan.push_back(Tree[id].conf);
    id = Tree[id].p_id;
  }

  int N = _plan.size();
  *planlength = N; *plan = (double**) malloc(N*sizeof(double*));
  for(int i=0; i<N; i++) { (*plan)[i] = _plan[N-1-i]; }

  return 1;
}


void MyPlanner::update_q_near(vector<node> _Tree, double* q){
  double d;
  q_near_id = 0;
  q_new_d = calc_dist(q,_Tree[q_near_id].conf);
  for(int i=1; i<_Tree.size(); i++){
    d = calc_dist(q,_Tree[i].conf);
    if (d<q_new_d){
      q_near_id = i;
      q_new_d = d;
    }
  }
}


int MyPlanner::get_q_new(double* q0, double* q1, double** p_q_new, int max_n_steps){
  int n_step = (int)(calc_dist(q0,q1)/step_size);
  update_q_step(q0,q1,n_step);
  (*p_q_new) = new double[numofDOFs];
  for(int i=1; i<max_n_steps+1; i++){
    for(int d=0; d<numofDOFs; d++) (*p_q_new)[d]=q0[d]+q_step[d]*i;
    if(is_valid(*p_q_new)){
      if(i==n_step+1) {return REACHED;}
    }
    else{
      if(i==1) {delete [](*p_q_new); return TRAPPED;}
      for(int d=0; d<numofDOFs; d++) (*p_q_new)[d]=q0[d]+q_step[d]*(i-1);
      break;
    }
  }
  return ADVANCED;
}



// RRT-Conect

int MyPlanner::rrtconnect_plan(double*** plan, int* planlength) {
  int i;
  double* q_new;
  int flag=TRAPPED;
  vector<node> *pTa, *pTb;
  int id;

  // init
  Tree.push_back({armstart_anglesV_rad,-1});
  Tree_r.push_back({armgoal_anglesV_rad,-1});

  // sample and build tree
  for(i=0;i<K;i++){
    gen_q_rand();
    if(i%2) { pTa=&Tree; pTb=&Tree_r; }
    else { pTa=&Tree_r; pTb=&Tree; }
    update_q_near((*pTa), q_rand);
    if(get_q_new((*pTa)[q_near_id].conf, q_rand, &q_new, MAX_N_STEPS)==TRAPPED) continue;
    (*pTa).push_back({q_new,q_near_id});
    update_q_near((*pTb), q_new);
    flag = get_q_new((*pTb)[q_near_id].conf, q_rand, &q_new, MAX_N_STEPS);
    if(flag==REACHED){break;}
    if(flag==ADVANCED){(*pTb).push_back({q_new,q_near_id});}
  }
  print_time("done sampling");
  if (i==K) { printf("Failed!\n"); return 0; }
  else { printf("Found! w/ %d samples.\n",i);}

  // output and format the plan
  vector<double*> _plan, _plan_b, _plan_a, *pp0, *pp1;
  id = q_near_id;
  while(id!=-1){
    _plan_b.push_back((*pTb)[id].conf);
    id = (*pTb)[id].p_id;
  }
  id = (*pTa).size()-1;
  while(id!=-1){
    _plan_a.push_back((*pTa)[id].conf);
    id = (*pTa)[id].p_id;
  }

  if(i%2) {pp0=&_plan_a; pp1=&_plan_b;}
  else {pp0=&_plan_b; pp1=&_plan_a;}
  int N0=(*pp0).size(), N1=(*pp1).size();

  *plan = (double**) malloc((N0+N1)*sizeof(double*));
  *planlength = N0+N1;
  for(int i=0;i<N0;i++)
    (*plan)[i] = (*pp0)[N0-1-i];
  for(int i=0;i<N1;i++)
    (*plan)[N0+i] = (*pp1)[i];

  return 1;
}



// RRT-Star

int MyPlanner::rrtstar_plan(double*** plan, int* planlength) {
  int i;
  double* q_new;
  int flag=TRAPPED;
  int max_n_steps=MAX_N_STEPS;

  // init
  Tree.push_back({armstart_anglesV_rad,-1});

  // sample and build tree
  for(i=0;i<K;i++){
    if(i%GoalAttemptCnt==0){
      for(int d=0; d<numofDOFs; d++) q_rand[d]=armgoal_anglesV_rad[d];
      max_n_steps = INF_STEPS;
    }
    else{
      gen_q_rand();
      max_n_steps = MAX_N_STEPS;
    }

    update_q_near(Tree, q_rand);
    flag = get_q_new(Tree[q_near_id].conf, q_rand, &q_new, max_n_steps);
    if(flag==TRAPPED) continue;
    Tree.push_back({q_new,q_near_id,q_new_d});
    q_new_id = Tree.size()-1;

    radius = get_radius(i);
    update_X_near(Tree,q_new,radius);
    rewire(Tree);
    if((i%GoalAttemptCnt==0)&&(flag==REACHED)) {break;}
  }
  print_time("done sampling");
  if (i==K) { printf("Failed!\n"); return 0; }
  else { printf("Found! w/ %d samples.\n",i);}

  // output and format the plan
  vector<double*> _plan;
  int id = Tree.size()-1;
  while(id!=-1){
    _plan.push_back(Tree[id].conf);
    id = Tree[id].p_id;
  }

  int N = _plan.size();
  *planlength = N;
  *plan = (double**) malloc(N*sizeof(double*));
  for(int i=0; i<N; i++) { (*plan)[i] = _plan[N-1-i]; }

  return 1;
}


void MyPlanner::rewire(vector<node> _Tree){
  int i, N=X_near_id.size();
  double Dist[N]={};
  double Cost[N]={};
  int Clear[N]={};
  double q_near_c, c;

  q_near_c = get_cost(_Tree,_Tree[q_new_id].p_id);
  q_new_c = q_near_c+_Tree[q_new_id].d;
  for(int ii=0; ii<N; ii++){
    i = X_near_id[ii];
    if(i==q_near_id){Dist[ii]=q_new_d; Cost[ii]=q_near_c; Clear[ii]=1; continue;}
    Clear[ii] = is_clear(_Tree[i].conf,_Tree[q_new_id].conf);
    if(!Clear[ii]) continue;
    Cost[ii] = get_cost(_Tree,i);
    Dist[ii] = calc_dist(_Tree[i].conf,_Tree[q_new_id].conf);
    c = Cost[ii]+Dist[ii];
    if (c<q_new_c){
      q_new_c = c;
      _Tree[q_new_id].p_id = i;
      _Tree[q_new_id].d = Dist[ii];
    }
  }

  for(int ii=0; ii<X_near_id.size(); ii++){
    if(!Clear[ii]) continue;
    i = X_near_id[ii];
    if(i==_Tree[q_new_id].p_id) {continue;}
    if(Cost[ii]>q_new_c+Dist[ii]){
      _Tree[i].p_id = q_new_id;
      _Tree[i].d = Dist[ii];
    }
  }
}



// PRM

int MyPlanner::prm_plan(double*** plan, int* planlength) {
  n_node = K/10;
  gen_roadmap();
  print_time("roadmap built");
  if(!astar_find_path()) return 0;

  int N = path_len;
  *planlength = N; *plan = (double**) malloc(N*sizeof(double*));
  for(int i=0; i<N; i++) { (*plan)[i]=roadmap[path.top()].conf; path.pop();}

  return 1;
}


void MyPlanner::gen_roadmap(){
  int i;  double* _q;

  q_start_id=n_node+0; q_goal_id=n_node+1;
	while(roadmap.size()<n_node+2){
    // get a sample
    radius=INF_RADIUS; // try to connect start_node and goal_node to all nodes, inf size of neighbor circle
    _q = new double[numofDOFs];
    switch (roadmap.size()-n_node) {
      case 1: for(int d=0; d<numofDOFs; d++) _q[d]=armgoal_anglesV_rad[d]; break; // add start
      case 0: for(int d=0; d<numofDOFs; d++) _q[d]=armstart_anglesV_rad[d]; break; // add goal
      default:
        radius=MAX_RADIUS; // limited size of neighbor circle
        gen_q_rand(); if(!is_valid(q_rand)) {delete[] _q; continue;}
        for(int d=0; d<numofDOFs; d++) _q[d]=q_rand[d];
    }
    //add the sample
    roadmap.push_back({_q});
    //connect to ALL nearby nodes (no filtering here)
    q_new_id = roadmap.size()-1;
    update_X_near(roadmap, q_rand, radius);
    for(int ii=0;ii<X_near_id.size();ii++){
      i = X_near_id[ii];
      if(is_clear(roadmap[i].conf,roadmap[q_new_id].conf)){
        roadmap[q_new_id].n_id.push_back(i);
        roadmap[i].n_id.push_back(q_new_id);
      }
    }
	}

}


int MyPlanner::astar_find_path(){
  int id0=0,id=0,idi=0;
  double Gi = 0;

  // Define parameters
  set<F_Id> openlist;
  unordered_set<int> closedlist;
  unordered_map<int,double> G;
  unordered_map<int,double>::iterator G_it;
  unordered_map<int,int> parent;

  // Initialize
  id0=q_start_id;
  G[id0]=0;
  openlist.insert( make_pair(0, id0) );

  // Expansion
  int cnt=0;
  int goal_reached=0;
  double c, hue;
  while(!openlist.empty()){
    cnt++;
    id = (*openlist.begin()).second;
    if(id==q_goal_id) {goal_reached=1;break;} // Goal expanded
    openlist.erase(openlist.begin());
    if(closedlist.count(id)) {continue;} // Already closed, skip expansion
    closedlist.insert(id);

    int n_succ=roadmap[id].n_id.size();
    for(int i=0;i<n_succ;i++){
      idi = roadmap[id].n_id[i];
      if(closedlist.count(idi)) {continue;} // if already closed

      c = calc_dist(roadmap[id].conf,roadmap[idi].conf);

      Gi = G[id] + c;
      G_it = G.find(idi);
      if(G_it==G.end()) {G[idi] = Gi;}  // not in open list
      else{                             // in open list
        if((G_it->second)>Gi) {G_it->second = Gi;}  // new G value is better, update
        else {continue;}                            // old G value is better, skip
      }
      hue = calc_dist(roadmap[q_goal_id].conf,roadmap[idi].conf);
      openlist.insert( make_pair(Gi+ASTAR_W*hue, idi) ); // ASTAR_W: set 1 for Dijkstra, 1 for A*, >1 for weighted A*
      parent[idi] = id;
    }
  }
  printf("%d expansions!\n", cnt);
  if(!goal_reached) return 0;

  // Back tracking
  path_len=1;
  while(id!=id0){
    path.push(id);
    id = parent[id];
    path_len++;
  }
  path.push(id0);

  return 1;
}



// Linear

int MyPlanner::linear_plan(double*** plan, int* planlength) {
  double distance = 0;
  for (int j = 0; j < numofDOFs; j++){
    if(distance < fabs(armstart_anglesV_rad[j] - armgoal_anglesV_rad[j])){
      distance = fabs(armstart_anglesV_rad[j] - armgoal_anglesV_rad[j]);
    }
  }
  int numofsamples = (int)(distance/step_size);
  if(numofsamples < 2){
    printf("the arm is already at the goal\n");
    return 0;
  }
  *plan = (double**) malloc(numofsamples*sizeof(double*));
  int firstinvalidconf = 1;
  int i,j;
  for (i = 0; i < numofsamples; i++){
    (*plan)[i] = (double*) malloc(numofDOFs*sizeof(double));
    for(j = 0; j < numofDOFs; j++){
      (*plan)[i][j] = armstart_anglesV_rad[j] + ((double)(i)/(numofsamples-1))*(armgoal_anglesV_rad[j] - armstart_anglesV_rad[j]);
    }
  }
  *planlength = numofsamples;
}
