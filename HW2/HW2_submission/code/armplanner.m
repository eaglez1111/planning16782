function[armplan,l,f,t,s,n,c] = armplanner(envmap, armstart, armgoal, planner_id)
%call the planner in C
tic
[armplan, armplanlength] = planner(envmap, armstart, armgoal, planner_id);
t = toc;

l = size(armplan);
l = l(1);

f=0;
s=0;
n=0;
c=0;
if l>1
    f=1;
    
    if t<5
        s=1;
    else
        s=0;
    end

    n=armplanlength;

    c = 0;
    for i=1:size(armplan)-1
        diff =[];
        for j = 1:length(armstart)
            diff(j) = mod(armplan(i+1,j)-armplan(i,j)+pi+8*pi,2*pi)-pi;
        end
        c = c + sqrt(sum(diff.^2));
    end    
end



% fprintf("n_spl:%d\n",armplanlength);